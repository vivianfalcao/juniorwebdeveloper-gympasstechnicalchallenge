Rails.application.routes.draw do
  

  devise_for :users,  :has_many => :addresses, :controllers => {registrations: 'registrations'}

  resources :users do
  	resources :addresses
  end
  
  resources :tokens 
    
  resources :gyms do
  	resources :addresses
  end
    
    #resources :requests, defaults: { format: 'json' }
    get 'home/index' => 'home#index'

    root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
