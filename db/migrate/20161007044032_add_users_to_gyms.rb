class AddUsersToGyms < ActiveRecord::Migration[5.0]
  def change
    add_reference :gyms, :user, foreign_key: true, null: false
    
  end
end
