class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.integer :types
      t.string :zip_code
      t.string :street
      t.string :city
      t.string :state
      t.float :longitude
      t.float :latitude
      t.belongs_to :addressable, :polymorphic => true
      
      t.timestamps
    end
    add_index :addresses, [:addressable_id, :addressable_type]
  end
end
