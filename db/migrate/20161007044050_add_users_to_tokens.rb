class AddUsersToTokens < ActiveRecord::Migration[5.0]
  def change
    add_reference :tokens, :user, foreign_key: true, null: false
  end
end
