class AddGymsToTokens < ActiveRecord::Migration[5.0]
  def change
    add_reference :tokens, :gym, foreign_key: true
  end
end
