class CreateGyms < ActiveRecord::Migration[5.0]
  def change
    create_table :gyms do |t|
      t.string :name, null: false
      t.boolean :status
      t.time :opening_time, null: false
      t.time :closing_time, null: false
      t.index :name, unique: true

      t.timestamps
    end
  end
end
