class GymsController < ApplicationController

  before_action :set_gym, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user! #except: [:index]
  
  # GET /gyms
  # GET /gyms.json
  def index
    if current_user.admin?
      @gyms = Gym.all.select{ |g| g.user_id == current_user.id }
    elsif current_user.gympass?
      @gyms = Gym.all
    end
  end

  # GET /gyms/1
  # GET /gyms/1.json
  def show
    @user = User.find(current_user.id)
    if  @user.regular?
        flash[:notice] = "Usuário sem permissão para executar a ação"
        redirect_to :controller => 'home'
    end
  end

  # GET /gyms/new
  def new
    if current_user.admin? || current_user.gympass? 
      @gym = Gym.new
      @gym.build_address
    else
      flash[:alert] = "Usuário sem permissão para executar a ação"
      redirect_to(request.referrer || root_path)
    end
  end

  # GET /gyms/1/edit
  def edit
    @user = User.find(current_user.id)
    if  @user.regular?
        flash[:notice] = "Usuário sem permissão para executar a ação"
        redirect_to :controller => 'home'
    end
  end

  # POST /gyms
  # POST /gyms.json
  def create
    
    if current_user.admin? || current_user.gympass? 
      @gym = Gym.new(gym_params)
      @gym.user_id = current_user.id
      @gym.status = false
      @gym.address.types = 1
      respond_to do |format|
        if @gym.save
          #@gym.create_address
          @users = User.all
          ConfirmationMailer.confirmation_email(@gym, @users).deliver_now
              
          format.html { redirect_to @gym, notice: 'Academia cadastrada com sucesso.' }
          format.json { render :show, status: :created, location: @gym }
        else
          format.html { render :new }
          format.json { render json: @gym.errors, status: :unprocessable_entity }
        end
      end
    else
      flash[:alert] = "Usuário sem permissão para executar a ação"
      redirect_to(request.referrer || root_path)
    end
  end

  # PATCH/PUT /gyms/1
  # PATCH/PUT /gyms/1.json
  def update
    @user = User.find(current_user.id)
    if  @gym.user_id == @user.id || @user.gympass?
      respond_to do |format|
        if @gym.update(gym_params)
          format.html { redirect_to @gym, notice: 'Academia atualizada com sucesso.' }
          format.json { render :show, status: :ok, location: @gym }
        else
          format.html { render :edit }
          format.json { render json: @gym.errors, status: :unprocessable_entity }
        end
      end
    else
        flash[:notice] = "Usuário sem permissão para executar a ação"
        redirect_to :controller => 'home'
    end
    
  end

  # DELETE /gyms/1
  # DELETE /gyms/1.json
  def destroy
    @user = User.find(current_user.id)
    if  @gym.user_id == @user.id || @user.gympass?
        @gym.destroy
        respond_to do |format|
          format.html { redirect_to gyms_url, notice: 'Academia deletada com sucesso' }
          format.json { head :no_content }
        end
    else
      flash[:alert] = "Usuário sem permissão para executar a ação"
      redirect_to(request.referrer || root_path)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gym
      @gym = Gym.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gym_params
        params.require(:gym).permit!
    end

end
