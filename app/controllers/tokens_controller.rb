class TokensController < ApplicationController
  before_action :set_token, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /tokens
  # GET /tokens.json
  def index
    if current_user.regular?
      if Token.where('DATE(created_at) =?', Date.today).where(:user_id => current_user.id).first_or_create!(:code => "TK" + (current_user.id).to_s + 
        Date.today.strftime("%d%m%Y"), :status => true, :user_id => current_user.id)
      end
    end
      @q = Token.ransack(params[:q])     
      @tk = @q.result     
      @tokens_user = Token.all.select{ |t| t.user_id == current_user.id }

    if(current_user.admin?)
      @adminTokens = Token.connection.select_all "SELECT tokens.status, tokens.code, tokens.id, tokens.user_id, tokens.gym_id, gyms.name FROM tokens JOIN gyms on tokens.gym_id = gyms.id;"
    
    end
  end

  def show
    redirect_to(request.referrer || token_path)
  end

  # GET /tokens/1/edit
  def edit
  end
  # PATCH/PUT /tokens/1
  # PATCH/PUT /tokens/1.json
  def update
      if current_user.gympass? || current_user.admin?
        @token.status = false
        if @token.update(token_params)
          flash[:alert] = "Token utilizado com sucesso"
          redirect_to(request.referrer || token_path)
        else
          format.html { render :index }
          format.json { render json: @token.errors, status: :unprocessable_entity }
        end
    end    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_token
      @token = Token.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def token_params
     params.require(:token).permit(:status, :code, :gym_id, :user_id)
     #params.require(:token).permit!
    end
end
