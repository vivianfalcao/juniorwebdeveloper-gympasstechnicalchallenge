class HomeController < ApplicationController
	 respond_to :html, :json

	def index
		
		@gyms = Gym.connection.select_all  "SELECT gyms.id, gyms.name, gyms.opening_time, 
		gyms.closing_time, gyms.user_id, gyms.status, addresses.street, addresses.city, addresses.state,
		addresses.latitude, addresses.longitude 
		FROM gyms, addresses WHERE addresses.addressable_id = gyms.id 
		AND addressable_type = 'Gym' AND gyms.status = true"

		if user_signed_in?
			@userAddresses = User.connection.select_all  "SELECT users.id, addresses.street, 
			addresses.city, addresses.latitude, addresses.longitude 
			FROM users, addresses WHERE addresses.addressable_id = users.id 
			AND addressable_type = 'User' AND users.id = #{current_user.id}"
						
		end
		respond_with @gyms
	end
end
