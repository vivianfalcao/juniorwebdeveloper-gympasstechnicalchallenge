class AddressesController < ApplicationController

  # POST /users
  # POST /users.json
  def create    
    @addressable = find_addressable
    @address = @addressable.addresses.build(address_params)
  end

  private
  def find_addressable
    params.each do |name, value|
      if name =~ /(.+)_id$/
          return $1.classify.constantize.find(value)
      end
    end
    nill
  end

end