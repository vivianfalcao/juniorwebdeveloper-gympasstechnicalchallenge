class ApplicationMailer < ActionMailer::Base
  default from: 'teste.email.confirmacao@gmail.com'
  layout 'mailer'
end
