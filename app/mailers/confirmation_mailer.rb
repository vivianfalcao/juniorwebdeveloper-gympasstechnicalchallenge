class ConfirmationMailer < ApplicationMailer
	default from: 'teste.email.confirmacao@gmail.com'

	def confirmation_email(gym, users)
		@users = users
		@gym = gym
		@users.each do |u|
			if u.gympass?		
				mail(to: u.email, subject: 'Confirmar cadastro de Academia')		
			end
		end
	end
end
