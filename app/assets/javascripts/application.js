// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require cocoon
//= require underscore
//= require gmaps/google
//= require_tree .


function sortGyms(type){

  if(type == 'search')
  {
    if (document.getElementById("location").value == '')
    {
      alert('Informe um endereço válido')
    }
    else
    {
      var address = document.getElementById("location").value;
    }
  }
  else
  {
    var address = document.getElementById("userAddress").value;   
  }

  $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address='+address+'&sensor=false', {
    format: 'json',
  }, function (results) {
    var lat = results.results[0].geometry.location.lat;
    var lon = results.results[0].geometry.location.lng;

    $.ajax({ 
    url: 'home/index',
    type: 'GET',
    dataType: 'json',
    success: function(data){    
     
      for (i = 0; i < data.length; i++) 
      {
        distance = google.maps.geometry.spherical.computeDistanceBetween(
        new google.maps.LatLng(data[i].latitude,data[i].longitude), 
        new google.maps.LatLng(lat, lon));
        
        var cell = document.getElementById(data[i].name);
        cell.innerHTML = (distance*0.001).toFixed( 2 )+' KM';
       }
      sortTable(); 
      }
    });
  });    
}

function sortTable(){ 

  var rows = $('#listGyms tbody tr').get();
  rows.sort(function(a, b){
    var A = parseFloat($(a).children('td').eq(5).text().replace(' KM', ''));
    var B = parseFloat($(b).children('td').eq(5).text());
    if(A < B){
      return - 1;
    }
    if(A > B){
      return 1;
    }
    return 0;
  }); 

  $.each(rows, function(index, row){ 
    $('#listGyms').children('tbody').append(row);
  });
}


function addressMap(type, count){
  $(document).ready(function() {

    (function (window, google, mapster) {
    
      //map option 
      var options = mapster.MAP_OPTIONS;  
       if(type == 'gym')
      {
          var element = document.getElementById("map-canvas");
      }
      else
      {
        var element = document.getElementById("map-canvas"+count);
      }
        
      // //map 
      var map = mapster.create(element, options); 

      var lat = '';
      var lon = ''; 
      var local; 
      
      google.maps.event.addListener(map.gMap, 'click', function (e)
      { 
        local = ((e.latLng).toString()).split(',');
        lat = local[0].replace('(', '');
        lon = local[1].replace(')', '');
        if(type == 'gym')
        {
          document.getElementById('gym_address_attributes_latitude').value = lat;
          document.getElementById('gym_address_attributes_longitude').value = lon;  
        }
        else
        {
          document.getElementById('user_addresses_attributes_'+count+'_latitude').value = lat;
          document.getElementById('user_addresses_attributes_'+count+'_longitude').value = lon;   
        }
        
        reverseGeocode(lat,lon, type, count);
       
      });
    }(window, google, window.Mapster));
  });
}


function geocode(address, type, count){
  
 $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address='+address+'&sensor=false', {
      format: 'json',
  }, function (results) {
    
    var lat = results.results[0].geometry.location.lat;
    var lon = results.results[0].geometry.location.lng;

    if(type == 'gym')
    {
      document.getElementById('gym_address_attributes_latitude').value = lat;
      document.getElementById('gym_address_attributes_longitude').value = lon;  
    }
    else
    {
      document.getElementById('user_addresses_attributes_'+count+'_latitude').value = lat;
      document.getElementById('user_addresses_attributes_'+count+'_longitude').value = lon;   
    }
 });
}

function reverseGeocode(lat, lon, type, count){
  $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lon+'&sensor=false', {
      format: 'json',
  }, function (results) {
      
      var street = results.results[0].address_components[1].long_name;
      var zip_code = results.results[0].address_components[7].long_name;
      var city = results.results[0].address_components[3].long_name;
      var state = results.results[0].address_components[5].short_name;       

      if(type == 'gym')
      { 
        document.getElementById('gym_address_attributes_street').value = street;
        document.getElementById('gym_address_attributes_zip_code').value = zip_code;  
        document.getElementById('gym_address_attributes_city').value = city;
        document.getElementById('gym_address_attributes_state').value = state;  
      }
      else
      { 
        document.getElementById('user_addresses_attributes_'+count+'_street').value = street;
        document.getElementById('user_addresses_attributes_'+count+'_zip_code').value = zip_code;  
        document.getElementById('user_addresses_attributes_'+count+'_city').value = city;
        document.getElementById('user_addresses_attributes_'+count+'_state').value = state;  
      }
  }); 
}

function gymMap(){ 
  $(document).ready(function() {
    var element = document.getElementById("map-canvas"); 
    var map = new google.maps.Map(element, {
          zoom: 10,
          center: {lat: -23.5505, lng: -46.6333}
        });
    
  $.ajax({ 
    url: 'home/index',
    type: 'GET',
    dataType: 'json',
    success: function(data){
      
      for (var i = data.length - 1; i >= 0; i--) {
          setMarker(data[i].latitude, data[i].longitude, data[i].name );
       }
    }
  });

  function setMarker(latitude, longitude, title)    {
    var marker = new google.maps.Marker({
      position: {
        lat: latitude,
        lng: longitude
      },
      title: title,
      map: map
    });  
  }     
}); 
}

function gympassProfile(){
  if((document.getElementById("user_email").value).endsWith("@gympass.com")){
  	document.getElementById("label_profile3").style.display = 'block ';
  	document.getElementById("label_profile3").style.marginLeft = '30px';
  	document.getElementById("user_profile_gympass").style.display = 'block';
  	document.getElementById("user_profile_gympass").checked = true;

  	document.getElementById("user_addresses_attributes_0_types_1").checked = true;
  	document.getElementById("user_addresses_attributes_0_street").value = "Av. Brigadeiro Faria Lima, 1306, cj 21";
  	document.getElementById("user_addresses_attributes_0_zip_code").value = "01451-001";
  	document.getElementById("user_addresses_attributes_0_city").value = "São Paulo";
  	document.getElementById("user_addresses_attributes_0_state").value = "SP";

  }
  else{
	document.getElementById("label_profile3").style.display = 'none';
  	document.getElementById("label_profile3").style.marginLeft = '30px';
  	document.getElementById("user_profile_gympass").style.display = 'none';
  	document.getElementById("user_profile_gympass").checked = false;

  	document.getElementById("user_addresses_attributes_0_types_1").checked = false;
  	document.getElementById("user_addresses_attributes_0_street").value = "";
  	document.getElementById("user_addresses_attributes_0_zip_code").value = "";
  	document.getElementById("user_addresses_attributes_0_city").value = "";
  	document.getElementById("user_addresses_attributes_0_state").value = "";
  }
}

  
