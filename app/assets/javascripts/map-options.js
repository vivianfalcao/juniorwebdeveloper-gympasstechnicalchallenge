(function (window, google, mapster) {
	
	mapster.MAP_OPTIONS = {
		center: {
			lat: -23.5505,
			lng: -46.6333		
		},
		zoom: 10,
		draggable: true,
		mapTupeId: google.maps.MapTypeId.ROADMAP	
	}; 
}(window, google, window.Mapster || (window.Mapster = {})))