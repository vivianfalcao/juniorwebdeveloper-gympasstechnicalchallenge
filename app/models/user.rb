class User < ApplicationRecord
	enum profile: [:regular, :admin, :gympass]

	has_many :addresses, :as => :addressable, :dependent => :destroy
	has_many :tokens
	accepts_nested_attributes_for :addresses

	devise :database_authenticatable, :registerable,
	        :recoverable, :rememberable, :trackable, :validatable

	validates :email, presence: true, uniqueness:true,
				 format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }


	if "profile == 0 "
		validates :addresses, presence: true, :length => { :minimum => 2 }		
	elsif "profile == 2"
		validates :addresses, presence: true
	else
		validates :addresses, presence: true
	end

	def with_addresses
    	self.addresses.build
    	self
  	end

end
