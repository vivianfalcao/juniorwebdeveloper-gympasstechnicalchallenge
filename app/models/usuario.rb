class User < ApplicationRecord
	has_many :addresses, :as => :addressable
	has_many :tokens

	accepts_nested_attributes_for :addresses

	validates :email, presence: true, uniqueness:true,
				 format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
	validates :name, presence: true
	validates :profile, presence: true
	validates :password, presence: true
	

end
