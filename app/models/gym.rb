class Gym < ApplicationRecord
  belongs_to :user
  belongs_to :token
  has_one :address , as: :addressable, :dependent => :destroy
  
  accepts_nested_attributes_for :address

  	validates :name, presence: true, uniqueness:true
	validates :opening_time, presence: true
	validates :closing_time, presence: true
	validates :user_id, presence: true

end
