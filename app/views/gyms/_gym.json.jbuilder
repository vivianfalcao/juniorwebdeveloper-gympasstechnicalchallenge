json.extract! gym, :id, :name, :opening_time, :closing_time, :user_id, :created_at, :updated_at
json.url gym_url(gym, format: :json)