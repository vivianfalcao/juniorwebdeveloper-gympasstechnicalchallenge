json.extract! token, :id, :status, :user_id, :created_at, :updated_at
json.url token_url(token, format: :json)