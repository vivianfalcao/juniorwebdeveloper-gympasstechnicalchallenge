# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


### DESENVOLVIMENTO DO PROJETO: 

O projeto foi desenvolvido com as seguintes versões:
# ruby 2.2.2p95
# Rails 5.0.0.1


Devido à falta de familiaridade com a linguagem, os dois primeiro dias (30/09 e 01/10) do projeto foram destinados ao desenvolvimento do projeto foram destinados à ambientação e estudos da linguagem. Tais estudos foram realizados com ajuda do mini curso:

# https://www.youtube.com/playlist?list=PLe3LRfCs4go-mkvHRMSXEOG-HDbzesyaP

Além dessas, sempre que necessário foram realizadas consultas nos seguintes sites:

# http://api.rubyonrails.org/
# http://apidock.com/rails

Foram realizadas consultas frequentes ao canal:
# https://www.youtube.com/user/RailscastsReloaded


Apesar de ter aprendido muito sobre a linguagem, enfrentei muitos problems por não conhecer bem a tecnologia, o que ocasionou em atrasos e retrabalho. Por exemplo, posso citar a associação polimórfica de endereços e as models users e gyms. A principio, tentei construir esta relação por meio de uma tabela de associação entre endereço e usuários sem que houvesse uma tabela para academia, neste caso o endereço era salvo na propria tabela academia (GYMS). Após agumas pesquisas, decidi que estas associações seriam mais bem feitas por polymorphic association. Porém, enfrentei muitos problemas com a utilização de nested_attributes e não consegui fazer com que esta solicitação funciona-se corretamente. Foram tentadas diversas soluções encontradas em forums ou nas fontes já citadas, porém sem exito, decidi voltar para a idéia original. Porém, retornei à associação polimórfica após aconselhamento do Adriano Mitre (Gympass). Cabe notar que encontrei pouca referencia que falasse especificamente de Rails 5 (versão utilizada) o que me faz pensar que pode se tratar de um problema relacionado à versão, pois segui a risca as sugestões e tutorias encontrados (também o material indicado pelo Adriano) e mesmo assim não consegui encontrar o problema. Ao verificar os registros do servidor pelo terminal consta que os dados são enviados, porém recebo o seguinte erro: "Unpermitted parameter: addresses_attributes". Conforme novo aconselho do Adriano, pesquisei sobre Strong Parameters e constatei que este realmente era o problema. Consegui solucionar o problema seguindo o proposto pelo seguinte video:
# https://www.youtube.com/watch?v=MKTIig3v_H0

Após solucionar o problema com a aceitação de addresses_attributes o Devise deixo de construir o endereço, isso foi solucionado com a criação do método "def with_addresses" na Model User.rb, conforme sugestão do forum:
http://stackoverflow.com/questions/3544265/how-do-i-use-nested-attributes-with-the-devise-model

Outro exemplo ocorreu com a utilização da gem Devise, a principio criei um scaffold Users e comecei a pesquisar para implemntar a autenticação de usuários por desse scaffold, porém após conhecimento da gem decidi modificr o projeto e utiliza-lá, encontrei alguns pequenos problemas com a mesma, mas acredito que foram solucionados.

Para implementar os mapas utilizados no sistema tentei utilizar as gems 'gmaps4rails' e 'geocoder', porém após algumas dificuldades (o sistema não reconhecia o 'geocoder') e devido à falta de tempo, decidi ealizar esta implementação por API do Google para javascript.

Caso houvesse mais tempo disponível, gostaria de ter implementado a parte de testes do sistema, melhorado questões de validação, utilizado a gem Pundit, que só conheci agora no final do projeto, para implementar restrições de acesso dos usuários.