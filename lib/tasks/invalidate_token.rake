namespace :invalidate do
  desc 'invalidates tokens that expired at end of the day'
  task :tokens => :environment do
    Token.where('date < ?', Time.now.to_date).each do |p|
      p.status = false
    end
  end
end